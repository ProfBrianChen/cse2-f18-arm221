///// Arnella Margolin
/// CSE 02 Lab 02 9/6/18
// bicycle cyclometer records time and number of rotations of front wheel

public class Cyclometer {
  
  public static void main(String [] args) {
    // our input data
    int secsTrip1=480; // stores seconds for first trip
    int secsTrip2=3220; // stores seconds for second trip
    int countsTrip1=1561; // counts rotation for first trip
    int countsTrip2=9037; // counts rotation for second trip
    
    // our intermediate variables and output data
    double wheelDiameter=27.0; // diameter of front bike wheel 
    double PI=3.14159;   // value of pi
    int feetPerMile=5280; // how many feet are in a mile
    int inchesPerFoot=12; // how many inches are in a foot
    int secondsPerMinute=60; //how many seconds are there per minute
    double distanceTrip1, distanceTrip2, totalDistance; //calucating distance of trips using pervious inputs
    
     System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    
    // calucating distance of the trip by how many rotations of the wheel times the diameter of the wheel
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
    
    //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
 


  }
}
