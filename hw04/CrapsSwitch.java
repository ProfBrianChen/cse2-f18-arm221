//HW 4 Craps Switch Program
// Arnella Margolin
// this program is the game of craps using switch instead of ifs

import java.util.Scanner;

public class CrapsSwitch{
  
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Do you want to roll random die(1) or pick two die number(2)?(Enter 1 or 2): ");
    int pick = myScanner.nextInt();
    
    int diceNum1;
    int diceNum2; 
    
    switch (pick){
      case 1: pick = 1;
      diceNum1 = (int)(Math.random()*6)+1; //generates the random dice numbers if user picks random generated number
      diceNum2 = (int)(Math.random()*6)+1;
        break;
      default:
      System.out.print("Enter the dice roll (1 to 6): "); // allows the user to pick the two dice
     diceNum1 = myScanner.nextInt();
      System.out.print("Enter the dice roll (1 to 6): ");
     diceNum2 = myScanner.nextInt();
        break; 
    }
    
    // assigns slag name to two dice roll totals 
    switch (diceNum1){
      case 1:
        switch (diceNum2){
          case 1:
           System.out.println("You got Snake Eyes");
           break; 
          case 2:
           System.out.println("You got Ace Deuce");
           break;
          case 3:
           System.out.println("You got Easy Four");
           break; 
          case 4:
           System.out.println("You got Fever Five");
           break; 
          case 5:
           System.out.println("You got Easy Six");
           break;
          case 6:
           System.out.println("You got Seven Out");
           break; 
       }
        break;
      case 2:
        switch (diceNum2){
          case 1:
            System.out.println("You got Ace Deuce");
            break; 
          case 2:
            System.out.println("You got Hard Four");
            break; 
          case 3:
            System.out.println("You got Fever Five");
            break; 
          case 4:
            System.out.println("You got Easy Six");
            break; 
          case 5:
            System.out.println("You got Seven Out");
            break; 
          case 6:
            System.out.println("You got Easy Eight");
            break; 
        }
        break;
      case 3:
        switch (diceNum2){
          case 1:
            System.out.println("You got Easy Four");
            break; 
          case 2:
            System.out.println("You got Fever Five");
            break; 
          case 3:
            System.out.println("You got Hard Six");
            break; 
          case 4:
            System.out.println("You got Seven Out");
            break; 
          case 5:
            System.out.println("You got Easy Eight");
            break; 
          case 6:
            System.out.println("You got Nine");
            break; 
        }
        break;
      case 4:
        switch (diceNum2){
          case 1:
            System.out.println("You got Fever Five");
            break; 
          case 2:
            System.out.println("You got Easy Six");
            break; 
          case 3:
            System.out.println("You got Seven Out");
            break; 
          case 4:
            System.out.println("You got Hard Eight");
            break; 
          case 5:
            System.out.println("You got Nine");
            break; 
          case 6:
            System.out.println("You got Easy Ten");
            break; 
        }
        break;
      case 5:
        switch (diceNum2){
          case 1:
            System.out.println("You got Easy Six");
            break; 
          case 2:
            System.out.println("You got Seven Out");
            break; 
          case 3:
            System.out.println("You got Easy Eight");
            break; 
          case 4:
            System.out.println("You got Nine");
            break; 
          case 5:
            System.out.println("You got Hard Ten");
            break; 
          case 6:
            System.out.println("You got Yo-leven");
            break; 
        }
        break;
      case 6:
        switch (diceNum2){
          case 1:
            System.out.println("You got Seven Out");
            break; 
          case 2:
            System.out.println("You got Easy Eight");
            break; 
          case 3:
            System.out.println("You got Nine");
            break; 
          case 4:
            System.out.println("You got Easy Ten");
            break; 
          case 5:
            System.out.println("You got Yo-leven");
            break; 
          case 6:
            System.out.println("You got Boxcars");
            break; 
        }
        break;
}
}
}