//HW 4 Craps If Program
// Arnella Margolin
// this program is the game of craps 

import java.util.Scanner;

public class CrapsIf{
  
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Do you want to roll random die(1) or pick two die number(2)?(Enter 1 or 2): ");
    double pick = myScanner.nextDouble();
    int diceNum1;
    int diceNum2; 
    
    if (pick == 1){
      diceNum1 = (int)(Math.random()*6)+1; //generates the random dice numbers if user picks random generated number
      diceNum2 = (int)(Math.random()*6)+1;
    }
    else {
      System.out.print("Enter the dice roll (1 to 6): "); // allows the user to pick the two dice
     diceNum1 = myScanner.nextInt();
      System.out.print("Enter the dice roll (1 to 6): ");
     diceNum2 = myScanner.nextInt();
    }
    //generates the slang terminology of the total of the two dice rolls
    if (diceNum1==1 && diceNum2==1){
      System.out.println("You got Snake Eyes");
    }
    else if (diceNum1==1 && diceNum2==2){
      System.out.println("You got Ace Deuce");
    }
    else if (diceNum1==1 && diceNum2==3){
      System.out.println("You got Easy Four");
    }
    else if (diceNum1==1 && diceNum2==4){
      System.out.println("You got Fever Five");
    }
    else if (diceNum1==1 && diceNum2==5){
      System.out.println("You got Easy Six");
    }
    else if (diceNum1==1 && diceNum2==6){
      System.out.println("You got Seven Out");
    }
     else if (diceNum1==2 && diceNum2==1){
      System.out.println("You got Ace Deuce");
     }
    else if (diceNum1==2 && diceNum2==2){
      System.out.println("You got Hard Four");
    }
    else if (diceNum1==2 && diceNum2==3){
      System.out.println("You got Fever Five");
    }
    else if (diceNum1==2 && diceNum2==4){
      System.out.println("You got Easy Six");
    }
    else if (diceNum1==2 && diceNum2==5){
      System.out.println("You got Seven Out");
    }
    else if (diceNum1==2 && diceNum2==6){
      System.out.println("You got Easy Eight");
    }
    else if (diceNum1==3 && diceNum2==1){
      System.out.println("You got Easy Four");
    }
    else if (diceNum1==3 && diceNum2==2){
      System.out.println("You got Fever Five");
    }
    else if (diceNum1==3 && diceNum2==3){
      System.out.println("You got Hard Six");
    }
    else if (diceNum1==3 && diceNum2==4){
      System.out.println("You got Seven Out");
    }
    else if (diceNum1==3 && diceNum2==5){
      System.out.println("You got Easy Eight");
    }
    else if (diceNum1==3 && diceNum2==6){
      System.out.println("You got Nine");
    }
    else if (diceNum1==4 && diceNum2==1){
      System.out.println("You got Fever Five");
    }
    else if (diceNum1==4 && diceNum2==2){
      System.out.println("You got Easy Six");
    }
    else if (diceNum1==4 && diceNum2==3){
      System.out.println("You got Seven Out");
    }
    else if (diceNum1==4 && diceNum2==4){
      System.out.println("You got Hard Eight");
    }
    else if (diceNum1==4 && diceNum2==5){
      System.out.println("You got Nine");
    }
    else if (diceNum1==4 && diceNum2==6){
      System.out.println("You got Easy Ten");
    }
    else if (diceNum1==5 && diceNum2==1){
      System.out.println("You got Easy Six");
    }
    else if (diceNum1==5 && diceNum2==2){
      System.out.println("You got Seven Out");
    }
    else if (diceNum1==5 && diceNum2==3){
      System.out.println("You got Easy Eight");
    }
    else if (diceNum1==5 && diceNum2==4){
      System.out.println("You got Nine");
    }
    else if (diceNum1==5 && diceNum2==5){
      System.out.println("You got Hard Ten");
    }
    else if (diceNum1==5 && diceNum2==6){
      System.out.println("You got Yo-leven");
    }
    else if (diceNum1==6 && diceNum2==1){
      System.out.println("You got Seven Out");
    }
    else if (diceNum1==6 && diceNum2==2){
      System.out.println("You got Easy Eight");
    }
    else if (diceNum1==6 && diceNum2==3){
      System.out.println("You got Nine");
    }
    else if (diceNum1==6 && diceNum2==4){
      System.out.println("You got Easy Ten");
    }
    else if (diceNum1==6 && diceNum2==6){
      System.out.println("You got Yo-leven");
    }
    else if (diceNum1==6 && diceNum2==6){
      System.out.println("You got Boxcars");
    }
  }
}