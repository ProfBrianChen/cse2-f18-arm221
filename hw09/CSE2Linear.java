// Arnella Margolin CSE2 Hw 9
// this program gives practice with arrays and in searching single dimensional arrays
// user input CSE grade 

import java.util.Arrays; //importing arrays 
import java.util.Random; //importing random variables
import java.util.Scanner; //importing scanners

public class CSE2Linear{

	public static void main(String[] args) { //MAIN METHOD 
		Scanner scan = new Scanner(System.in); 
		
		System.out.println("Enter 15 ascending ints for final grades in CSE2: "); //Prompts user for 15 integers 
		
		String input = scan.nextLine();
		
		String[] gradeInput = input.split(" "); //splitting the string by spaces
		System.out.println(Arrays.toString(gradeInput)); //printing the array 
		int[] newArray = new int[gradeInput.length];  //the length of the array 
		for(int i = 0; i < gradeInput.length; i++) { //scoping through each element
		    newArray[i] = Integer.parseInt(gradeInput[i]); 
		}		    
    
		int i=0;
		while (i < newArray.length) { //making sure it is less than the length 
			if (newArray[i] < 0 || newArray[i] >100) { //ensuring that it is in the range
				System.out.println( "Error, integers must be between 1-100"); 
				break;
			}
			else {
				i++; //increasing the values 
			}
		}
		
		for (int j = 0; j < (gradeInput.length-1); j++) { //going through the length 
			if (newArray[j+1] <= newArray[j]) { //making sure it is ascending
				System.out.println("Error, the integers must be ascending"); 
				break;
			}
		}
		
		System.out.println("Enter a grade to search for: "); //prompting asking for the grade
		int a = scan.nextInt();
		System.out.println(binarySearch(a, newArray)); //binary search returning
		System.out.println("Scrambled: " + Arrays.toString(Scramble(newArray))); //returning the scramble

		System.out.println("Enter a grade to search for: "); //prompting asking for the grade
		int b = scan.nextInt();
		System.out.println(linearSearch(b, newArray)); //print the linear search array 

	
	} //end of main method 
	
	
//Method for Binary Search 
	
public static String binarySearch(int key, int[] newArray) { //arguments that you need to use in the method 
	int low =0;
	int high;
	int iterations = 0; //variable to count the number of iterations 
	high = newArray.length-1; //defining the top of the array is length minus 1
	
	while (high >= low ) { 	//while the top is great than the bottom 
		iterations+=1; //increase an iteration
		int mid = (low + high)/2; //define the middle as avg of top and bottom 
		if (key < newArray[mid]) { //if the number looking for is less than the middle
			high = mid - 1; //redefine as new top minus 1, because you never check that number twice
		}
		else if (key == newArray[mid]) {
			return "Your search was found, with " + iterations + " iteration(s)"; //if we find the number we're looking for print this 
		}
		else {
			low = mid + 1;
		}
	}
	return key + "was not found in the list with " + iterations + " iteration(s)";
	
} 


//Method for Scrambling Array 


public static int[] Scramble(int[] newArray) {
	
	int[] temp = new int[15];// defining a temporary array to hold the value of cards 
	Random randNum = new Random(); //creating a random number generator

	for (int i=0; i<15; i++){ //i goes from 0 --> 14
		int k = randNum.nextInt(15); //this is defining k as a random integer under 101 
		temp[k] = newArray[i]; //this is using the created array to hold the value of CSE grades
		newArray[i] = newArray[k]; //this is then taking that value and making it the new value
		newArray[k] = temp[k]; //And this is transferring it to the temporary array 
	
}
	return newArray;
	
} 

//Method for Linear Search 

public static String linearSearch(int key, int[] newArray) { //arguments that you need to use in the method 
	int iterations = 0; //variable to count the number of iterations 
	for (int i = 0; i < newArray.length; i++) 	{
		iterations+=1; //increase an iteration
		if (key == newArray[i]) 
			return "Your search was found, with " + iterations + " iteration(s)";
	}

	return key + "was not found in the list with " + iterations + " iteration(s)";

}

} 