//Arnella Margolin CSE2 Hw 9 Program 2
//gives you practice with arrays and in searching single dimensional arrays
//

import java.util.Arrays; //importing arrays
import java.util.Random; //importing random variables
import java.util.Scanner; //importing scanners

public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9] \n");
    num = randomInput();
  	String out = "The original array is: ";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
    out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2 = "The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
	

	
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  

  public static int[] randomInput() {
		int [] Array = new int[10];
		int i =0;
		for(i = 0; i < Array.length; i++) {
			 Array[i] = (int)(Math.random() * 9);
		}
		return Array;
	  
	  
  }
  
  //list is an array, position is an integer 
  
  public static int[] delete(int[] list, int pos) {
	  	if (pos >= 10 || pos < 0) {
	  		System.out.println("The index is not valid");
	  		return list;
	  	}
	    int [] list1 = new int[9]; //initializing the new array with 9 elements
		int i = 0; //i =0
		int hit = 0;//creating new variable named hit to do a switch  
		for(i = 0; i < list.length; i++) { //for loop going through each element 
			if (i == pos) { //if the element equal the pos function 
				hit = 1; //then hit is 1 -- turning a switch 
			}
			
			else { //if i does not equal pos function then... 
				if (hit == 0) { //if it equal zero 
					list1[i]=list[i]; //then you are replacing each element in the new array with something in the old array 
				}
				if (hit == 1) { // when hit equals 1 
					list1[i-1]=list[i]; //this shifts when comparing the new and old array 
				}
			}	  
	      
  }
		return list1;
  }
  
  public static int[] remove(int[] list, int target) {
	  int j = 0;
	  int counter =0;
	  int hit = 0;
	  for(j = 0; j < list.length; j++) { //for loop going through each element of original array  
			if (list[j] == target) { //if the number at j is equal to the target 
				counter ++; // adds to counter every time target is equal to the number of that element
			}
	  }
	  
	int newList[] = new int[list.length-counter]; //defining the new array as an array of length old array minus the counter 
	
	int i=0;
	for (i=0; i < list.length; i++) { //going through the variables of the old array 
		if (list[i] == target) {
			hit++; //increase hit every time it hits the target
		}
		else {
			newList[i-hit] = list[i];  //defining the new list as the old list minus hit which is each time it hits the target 
		}
}	
	  return newList; //returning the new array 
  } 
  
}
