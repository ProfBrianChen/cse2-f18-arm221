// Arnella Margolin
// Hw 7 CSE 2 Word Tools
// This homework has the objective of giving practice writing methods, manipulating strings and forcing the user to enter good input. 

import java.util.Scanner;

public class WordTools {
  
  public static String sampleText(Scanner scan){
    //Method for sample text. 

       String paraInput; // Creates a string variable for user input
        System.out.println("Enter a sample text:"); // Asks user to enter text
        paraInput = scan.nextLine(); // nextline function allows for whole paragraph
        System.out.println(); 
        System.out.println("You entered: " + paraInput); // Displays what the user wrote in the previous step
        System.out.println();// newline
        return paraInput; //Return allows us to use what the user input in this method to be used in the main method
  }
  
   public static String printMenu(Scanner scan) { //scan is used because this method requests a letter from the user

         System.out.println(); 
         System.out.println("Menu"); //The following print statements print out the menu to the user
         System.out.println("c - Number of non-whitespace characters");
         System.out.println("w - Number of words");
         System.out.println("f - Find text");
         System.out.println("r - Replaces all !'s");
         System.out.println("s - Shorten spaces");
         System.out.println("q - quit");
         System.out.println(); 
         System.out.println("Choose an option:"); //asks user to input a letter from the menu

        String userChoice = scan.next(); //Lets the user input a letter

        return userChoice; // Returns this letter to use in main method

 }
  
  public static int getNumOfNonWSCharacters(String paraInput) { //using what the user input to this method

        int count = 0; // counter
        char countCharac; 
          
              for( int i = 0; i < paraInput.length(); i = i+1 ){ //uses the function .lenght(); to see number of characters
                   countCharac = paraInput.charAt(i); // character is what integer i is at when it moves through text
                       if(countCharac != ' ') { // if loop for if the current character is not a space
                              count++; //Only increments the count if character isn't a space
                          }
                      }
                      return count; // returns the integer count to the main method
           }
  
   public static int getNumOfWords(String paraInput) {                      
       int wordAmount = 0; //Variable made for the number of words
       boolean correct = false; //boolean value if the character is a word
       int endCount = paraInput.length() - 1; //loop needs to count the last word
 
               for (int i = 0; i < paraInput.length(); i = i + 1) {
                      if (paraInput.charAt(i) == '\'') { // need to account for symbols that arent words 
                          wordAmount--;  
                      }

                   if (Character.isLetter(paraInput.charAt(i)) && i != endCount) { //If on a letter, then true
                      correct = true;                     
                   } 
                  
                   else if (!Character.isLetter(paraInput.charAt(i)) && correct) { // If comes across a non letter after finishing a word, it will increment the word count
                       wordAmount++; // increments wordcount
                       correct = false; // Returns false until another character is reached                
                   } 

                   else if (Character.isLetter(paraInput.charAt(i)) && i == endCount) { //reaches the last word since there is no space after the last word 
                       wordAmount++; 
               }
               
           }
     return wordAmount; //Returns to main method
   }
  
     public static int findText(String paraInput) { //Method for finding text
               Scanner scan = new Scanner(System.in); 
               System.out.println("Enter a word or phrase to be found:"); // Asks user for a word or phrase to be found

               String wordSearch = scan.nextLine(); 
               int numWords = 0; //Variable for amount a word is found
               numWords = paraInput.split(wordSearch).length;//Function for finding the word that the user searched within the original phrase
               
               wordSearch = "'" + wordSearch + "'";
               System.out.print(wordSearch); //since this method is an integer type, it can't return this string back to the main method, so it is printed here
               System.out.println("Instances " + numWords);   
               return numWords; //returns the number back to main method

           }
     
     public static void replaceExclamation(String paraInput) { //Void is before the method name because no values are being returned to main method
              char fixIt; //character needs to be changed
              StringBuilder changeString = new StringBuilder(paraInput); //string builder function to change a string
                     
                   for(int i = 0; i < paraInput.length(); i = i+1 ){
                       fixIt = paraInput.charAt(i);
                          if (fixIt == '!') { // is true when the character is an exclamation
                              changeString.setCharAt(i,'.'); //Changes the current string for a period
                          }
                      }
                      System.out.println(changeString); // Prints out the new string
       
       return;
           }

     public static void shortenSpace(String paraInput) { //Method for shortening spaces
             System.out.println(paraInput.replaceAll("\\s+", " ")); // Function that replaces all spaces over one space to just one space  
       return;
           }

      public static void main(String[] args) {
            Scanner scan = new Scanner(System.in); // Sets up scanner function in main method
            String paraInput = sampleText(scan); 
            int countChar = 0; // initializes variable
            int wordAmount = 0; //
            int numWords = 0; 

            String userInput = printMenu(scan); // Gives the string userInput the string returned from the printMenu method
            boolean runLoop = true; //Aribrary true value
                if (userInput.equals("q")) {  //Immediately quits, can happen before other options
                      }
                      else { // for all values other than q
                         while (runLoop) {
                          
                            if (userInput.equals("c")) {
                              countChar = getNumOfNonWSCharacters(paraInput); 
                              System.out.println("Number of non-whitespace characters: " + countChar); // print
                              runLoop = false; 
                            }

                           else if (userInput.equals("w")) {
                            
                              wordAmount = getNumOfWords(paraInput); 
                              System.out.println("Number of words: " + wordAmount); //print
                              runLoop = false;  
                            }

                            else if (userInput.equals("f")) {
                              
                              numWords = findText(paraInput); 
                              runLoop = false; 
  
                           } 
                           
                           
                      }
                }
      }    
}