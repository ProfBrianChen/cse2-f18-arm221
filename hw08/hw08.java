// Arnella Margolin
// CSE 2 Hw 8
// 

import java.util.Scanner;
import java.util.Random; 

public class hw08{ 
  
  public static void shuffle(String [] list){  
    System.out.println("Shuffled");
    Random randomGenerator = new Random(); //initializes random class

    for (int i = 0; i < 500; i ++){
      //finds random member to swap
      String empty = list[0];
      int mix = randomGenerator.nextInt(52);
      list[0] = list[mix];
      list[mix] = empty; 
    }

  }
  
  public static void printArray(String[] list) {
      for (int k = 0; k < list.length; k++) {
        System.out.print(list[k] + " ");
      }
    System.out.println();
   }
  
 public static String[] getHand(String[] cards, int index, int numCards) {

       System.out.println("Hand");
       String[] hand = new String[5];
       for (int i =0; i < numCards; i++) {
           hand[i] = cards[index];
           index--;
 }

       return hand;
 }
  
  public static void main(String[] args) { 
      Scanner scan = new Scanner(System.in); 
      //suits club, heart, spade or diamond 
      String[] suitNames={"C","H","S","D"};    
      String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
      String[] cards = new String[52]; 
      String[] hand = new String[5]; 
      int numCards = 5; 
      int again = 1; 
      int index = 51;
    
          for (int i=0; i<52; i++){ 
              cards[i]=rankNames[i%13]+suitNames[i/13]; 
              System.out.print(cards[i]+" "); 
} 
    
      System.out.println();
      printArray(cards); 
      shuffle(cards); 
      printArray(cards); 

    while(again == 1){ 
        hand = getHand(cards,index,numCards); 
        printArray(hand);
        index = index - numCards;
        System.out.println("Enter a 1 if you want another hand drawn"); 
        again = scan.nextInt(); 
}  
  } 
}
