//Arnella Margolin
//CSE Hw 5 
// print the number of loops as well as the probability associated with each of the four hands. 


import java.util.Scanner;

public class Hw05{

                // main method required for every Java program

               public static void main(String[] args) {

                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate inputs

                 boolean correct = true;

                 System.out.println("Enter the number of loops (100,1000,10000, or 1000000 suggested):");   // asks the user to input the number of loops

                 int amountLoop = 0;   // declare variable for number of loops

                 if (myScanner.hasNextInt()){

                   amountLoop = myScanner.nextInt(); // sets input equal to variable for number of loops

                 } else {

                   correct = false; // tests whether the input is the correct type

                 }

                 while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type

                   myScanner.next();

                   System.out.println("Error: wrong type! Enter the number of loops as an interger." ); //tells user there is an error with their type and asks them for new input

                   if (myScanner.hasNextInt()){ // checking to see if input is the right type

                     amountLoop = myScanner.nextInt();

                     correct = true; // if its true, it breaks out of the while loop

                 }

                 }

                 // Declare variables to be used throughout program.

                 int a = 1;

                 int card1;

                 int card2;

                 int card3;

                 int card4;

                 int card5;

                 int val1;

                 int val2;

                 int val3;

                 int val4;

                 int val5;

                 double four = 0;

                 double three = 0;

                 double twoPair = 0;

                 double onePair = 0;

                

                 // Run while loop for the amount of times the user inputed

                 while (a++ <= amountLoop){

                   // Generate and assign values for each of the 5 cards

                   card1 = (int)(Math.random()*52)+1;                 
                   val1 = (int)(card1%13);   // assign cards 1-13 in each suit to a card value            
                   card2 = (int)(Math.random()*52)+1;                
                   val2 = (int)(card2%13);

                   

                 if (card2 != card1){   // Make sure that the same number is not randomly picked more than once

                   card3 = (int)(Math.random()*52)+1;                

                   val3 = (int)(card3%13);

                  

                   card4 = (int)(Math.random()*52)+1;                

                   val4 = (int)(card4%13);

                  

                   card5 = (int)(Math.random()*52)+1;                

                   val5 = (int)(card5%13);

                 } else {

                   card2 = (int)(Math.random()*52)+1;                

                   val2 = (int)(card2%13);

                 }

                  

                   card3 = (int)(Math.random()*52)+1;                

                   val3 = (int)(card3%13);

                  

                 if (card3 != card1){   // Make sure that the same number is not randomly picked more than once

                   card4 = (int)(Math.random()*52)+1;                

                   val4 = (int)(card4%13);

                  

                   card5 = (int)(Math.random()*52)+1;                

                   val5 = (int)(card5%13);

                 } else {

                   card3 = (int)(Math.random()*52)+1;                

                   val3 = (int)(card3%13);

                 }               
                   card4 = (int)(Math.random()*52)+1;                 

                   val4 = (int)(card4%13);

                  

                 if (card4 != card1){   // Make sure that the same number is not randomly picked more than once

                   card5 = (int)(Math.random()*52)+1;                 

                   val5 = (int)(card5%13);

                 } else {

                   card4 = (int)(Math.random()*52)+1;                

                   val4 = (int)(card4%13);

                 }
                 
                   card5 = (int)(Math.random()*52)+1;                

                   val5 = (int)(card5%13);

                   
                 if (card5 == card1){   // Make sure that the same number is not randomly picked more than once

                   card5 = (int)(Math.random()*52)+1;                

                   val5 = (int)(card5%13);

                 }

                

                 // Count number of each hand occurring

                 if (((val1 == val2) && (val2 == val3) && (val3 == val4)) || ((val1 == val2) && (val2 == val3) && (val3 == val5)) || ((val1 == val2) && (val2 == val4) && (val4 == val5)) || ((val1 == val3) && (val3 == val4) && (val4 == val5)) || ((val2 == val3) && (val3 == val4) && (val4 == val5))){

                   four += 1;

                 } else if(((val1 == val2) && (val2 == val3)) || ((val1 == val2) && (val2 == val4)) || ((val1 == val2) && (val2 == val5)) || ((val1 == val3) && (val3 == val4)) || ((val1 == val3) && (val3 == val5)) || ((val3 == val4) && (val4 == val5)) || ((val2 == val3) && (val3 == val4)) || ((val2 == val3) && (val3 == val5)) || ((val1 == val4) && (val4 == val5)) || ((val2 == val4) && (val4 == val5))){

                   three += 1;

                 } else if (((val1 == val2) && (val3 == val4)) || ((val1 == val2) && (val3 == val5)) || ((val1 == val2) && (val4 == val5)) || ((val1 == val3) && (val2 == val5)) || ((val1 == val3) && (val2 == val4)) || ((val1 == val3) && (val4 == val5)) || ((val1 == val4) && (val2 == val3)) || ((val1 == val4) && (val2 == val5)) || ((val1 == val4) && (val3 == val5)) || ((val1 == val5) && (val2 == val3)) || ((val1 == val5) && (val2 == val4)) || ((val1 == val5) && (val3 == val4))){

                   twoPair += 1;

                 } else if ((val1 == val2) || (val1 == val3) || (val1 == val4) || (val1 == val5) || (val2 == val3) || (val2 == val4) || (val2 == val5) || (val3 == val4) || (val3 == val5) || (val4 == val5)){

                   onePair += 1;

                 }

                  

                 }      

  

                 // Divide the count of each hand by the amount of trials to find the probability of each occurring.  

                 double fourProb = four/amountLoop;

                 double threeProb = three/amountLoop;

                 double twoPairProb = twoPair/amountLoop;

                 double onePairProb = onePair/amountLoop;

                 // Print out the number of loops and the probabilities of each hand

                 System.out.println("The number of loops: " + amountLoop);

                 System.out.println("The probability of Four-of-a-kind: " + fourProb);

                 System.out.println("The probability of Three-of-a-kind: " + threeProb);

                 System.out.println("The probability of Two-pair: " + twoPairProb);

                 System.out.println("The probability of One-pair: " + onePairProb);

                

               }

 

}