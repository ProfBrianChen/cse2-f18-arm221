/// CSE 2 HW 2
// Arnella Margolin 

public class Arithmetic{
  
  public static void main(String args[]){
     //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

    // cost of each item
double totalCostOfPants;   //total cost of pants
    totalCostOfPants= numPants*pantsPrice;
double totalCostofShirts; //total cost of shirts
    totalCostofShirts=numShirts*shirtPrice;
double totalCostofBelts;  //total cost of belts
      totalCostofBelts=numBelts*beltCost;
    
    //taxes on each item
double pantTax; //tax on pants
    pantTax=totalCostOfPants*paSalesTax;
    pantTax=(int)(pantTax*100) / 100.0;
double shirtTax; //tax on shirts
    shirtTax=totalCostofShirts*paSalesTax;
    shirtTax=(int)(shirtTax*100) / 100.0;
double beltTax; //tax on belts 
    beltTax=totalCostofBelts*paSalesTax;
    beltTax=(int)(beltTax*100) / 100.0;
    
    //total cost before taxes
 double totalCost; 
    totalCost=totalCostOfPants+totalCostofShirts+totalCostofBelts; 
    
    //total sales tax
 double totalSalesTax; 
    totalSalesTax=pantTax+shirtTax+beltTax;
    totalSalesTax=(int)(totalSalesTax*100) / 100.0;
    
    //total cost of purchases
 double total;
    total=totalCost+totalSalesTax; 
    total=(int)(total*100) / 100.0;
    
    //print out data
    System.out.println("Pants cost $"+ totalCostOfPants);
    System.out.println("Shirts cost $"+ totalCostofShirts);
    System.out.println("Belts cost $"+ totalCostofBelts);
    System.out.println("Sales tax for pants is $"+ pantTax);
    System.out.println("Sales tax for shirts is $"+ shirtTax);
    System.out.println("Sales tax for belts is $"+ beltTax);
    System.out.println("Total before taxes is $"+ totalCost);
    System.out.println("Total Sales Tax is $" + totalSalesTax);
    System.out.println("Total after taxes is $"+ total);
    

  }
}