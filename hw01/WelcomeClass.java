//////////////
/// hw01 CSE 002 Welcome Class
/// Arnella Margolin 9/3/18 CSE 002-311
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Welcome and a short autobiographical to terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-A--R--M--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("Hi I'm Arnella Margolin. I'm in IBE studying Industrail Engineering and Finance. I spent the whole summer working an internship in Israel. I enjoy traveling, reading, and watching movies.");
    
  }
  
}
