// CSE 2 HW 3 Program 1
/// Arnella Margolin
// program determines number of acres of land that were affected by hurricane rain and how many inches of rain were dropped 

import java.util.Scanner;

public class Convert{
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the affected area in acres: "); //inputs the amount of acres affected by rainfall
    
    double acres = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in the affected area: "); //inputs the amount of rainfall
    
    double rainfall = myScanner.nextDouble();
    
    //compute affected times inches of rain and generates the amount in gallons 
    double totalAreaAffected;
    totalAreaAffected = ((acres) * (rainfall)) * 27154.2857; 
    double a = Math.pow(10,-13);
    
    // Convert quality of rain into cubic miles 
    double totalCubicMiles;
    totalCubicMiles = (9.08169 * a);
    
    // Output of affected area 
    double cubicMilesRain;
      cubicMilesRain = totalAreaAffected * totalCubicMiles;
    System.out.println("Total affected area is: "+ cubicMilesRain + " cubic miles");
    
   
  }
}