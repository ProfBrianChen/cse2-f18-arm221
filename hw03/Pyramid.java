/// CSE2 Hw 3 Program 2
// Arnella Margolin 
// prompts user for dimensions of a pyramid and returns the volume inside the pyramid

import java.util.Scanner;

public class Pyramid{
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("The square side of the pyramid is (input length): "); //input the side length of the pyramid
    
    double squareSide = myScanner.nextDouble();
    
    System.out.print("The height of the pyramid is (input height): ");
    double pyramidHeight = myScanner.nextDouble();
    
    
    
    //compute the area of the base
    double baseArea = Math.pow(squareSide, 2);
    double volume = ((baseArea * pyramidHeight)/ 3); 
    System.out.println("The volume inside the pyramid is: " + volume);
    
    
  }
}