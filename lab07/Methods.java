// Arnella Margolin
// Lab 07 CSE 2 

import java.util.Random; 

import java.util.Scanner; 

public class Methods {
  
           public static String adjectives() {  //method for picking adjectives

                      String Adj = "initialize"; 

                      Random randGen = new Random(); 

                      int randNum = randGen.nextInt(10); //randomly generates a number 
                    
                      switch (randNum) { //asigns adjective to the number that is chosen using a switch statement 

                      case 0: Adj = "slow";
                                 break; 

                      case 1: Adj = "yummy";
                                 break;

                      case 2: Adj = "annoyed";
                                 break;

                      case 3: Adj = "happy";
                                 break;

                      case 4: Adj = "agitated";
                                 break;

                      case 5: Adj = "hungry";
                                 break;

                      case 6: Adj = "short";
                                 break;

                      case 7: Adj = "fat";
                                 break;

                      case 8: Adj = "weak";
                                 break;

                      case 9: Adj = "exhausted";
                                 break;
                      }

                      return Adj; 
           }
 

           public static String subjectNoun() {  //method for picking noun
                      String Noun1 = "initialize"; 

                      Random randGen = new Random(); 

                      int randNum = randGen.nextInt(10);
                    
                      switch (randNum) {

                      case 0: Noun1 = "elephant"; 
                                 break; 
                          
                      case 1: Noun1 = "puppy";
                                 break;
                          
                      case 2: Noun1 = "hawk"; 
                                 break;

                      case 3: Noun1 = "zebra";
                                 break;

                      case 4: Noun1 = "penguin";
                                 break;

                      case 5: Noun1 = "bee"; 
                                 break;

                      case 6: Noun1 = "bear"; 
                                break;

                      case 7: Noun1 = "tiger"; 
                                 break;

                      case 8: Noun1 = "belt";
                                 break;

                      case 9: Noun1 = "computer";
                                 break;
                      }

                      return Noun1;
           }

          

           public static String pastTenseVerbs() { //method for picking verb

                      String Verb1 = "initialize";

                      Random randGen = new Random(); 

                      int randNum = randGen.nextInt(10);

                      switch (randNum) {

                      case 0: Verb1 = "biked up the"; 
                                 break; 

                      case 1: Verb1 = "blew on the";
                                 break;

                      case 2: Verb1 = "paraded the";
                                 break;

                      case 3: Verb1 = "swam to the";
                                 break;

                      case 4: Verb1 = "threw the";
                                 break;

                      case 5: Verb1 = "kissed the";
                                 break;

                      case 6: Verb1 = "ate the"; 
                                 break;

                      case 7: Verb1 = "married the";
                                 break;

                      case 8: Verb1 = "napped in the";
                                 break;

                      case 9: Verb1 = "flew to the";
                                 break;

                      }

                      return Verb1; 
           }

          

           public static String objectNoun() { //method for picking second noung

                      String Noun2 = "initialize";

                      Random randGen = new Random(); 

                      int randNum = randGen.nextInt(10);

                      switch (randNum) {

                      case 0: Noun2 = "lake"; 
                                 break; 

                      case 1: Noun2 = "bowl"; 
                                 break;

                      case 2: Noun2 = "microphone";
                                 break;

                      case 3: Noun2 = "brush";
                                 break;

                      case 4: Noun2 = "eye";
                                 break;

                      case 5: Noun2 = "purse"; 
                                 break;

                      case 6: Noun2 = "truck"; 
                                 break;

                      case 7: Noun2 = "boat"; 
                                 break;

                      case 8: Noun2 = "mug"; 
                                 break;

                      case 9: Noun2 = "unicorn";
                                 break;

                      }

                      return Noun2;
           }

          

           public static String placementNoun(String subjectNoun) { //writes out the beginning of the sentence 

                      Random randGen = new Random(); 

                      int randNum = randGen.nextInt(2);                     

                      if (randNum == 0) { //randomly picks how the sentence starts 

                        subjectNoun = "It ";
                      }
                      else {
                        
                        subjectNoun = "The " + subjectNoun + " ";
                      }
                      return subjectNoun; 
           }

          
           public static void conclusion(String subjectNoun) {

                      System.out.print("That " + subjectNoun); 

                      System.out.print(" " + pastTenseVerbs() + " ");

                      System.out.println(objectNoun() + "!");

           }

          

           public static void finalMethod() {
                    
                      Random randGen = new Random(); 

                      int randNum = randGen.nextInt(3) + 2;

                     

                      String adj1 = "Initialized"; 

                      String adj2 = "Initialized";

                      String subjectNoun = "Initialized";

                      String subjectNoun2 = "Initialized";

                      String objNoun = "Initialized";

                      String objNoun2 = "Initialized";

                     

                      System.out.print("The "); 

                      adj1 = adjectives(); 

                      System.out.print(adj1); 

                      adj2 = adjectives(); 

                      while (adj1 == adj2) { 

                                 adj2 = adjectives(); 

                      }

                      System.out.print(" " + adj2); 

                      subjectNoun = subjectNoun(); 

                      System.out.print(" " + subjectNoun); 

                      System.out.print(" " + pastTenseVerbs()); 

                      System.out.println(" " + objectNoun() + "."); 

                      for (int i = 0; i < randNum; i++) {  

                      subjectNoun2 = placementNoun(subjectNoun); 

                      System.out.print(subjectNoun2); 

                      System.out.print(pastTenseVerbs()); 

                      objNoun = objectNoun(); 

                      System.out.print(" " + objNoun);

                      System.out.print(" at the"); 

                      System.out.print(" " + adjectives());

                      objNoun2 = objectNoun();

                      while (objNoun == objNoun2) { 

                                 objNoun2 = objectNoun();
                      }

                      System.out.println(" " + objNoun2 + ".");

                      }

                      conclusion(subjectNoun); 
           }

 

           public static void main(String[] args) {

                      Scanner scan = new Scanner(System.in);

                      int input = 1;  

                      String adj1 = "Initialized"; 

                      String adj2 = "Initialized";

                      String subjectNoun = "Initialized";

                      String subjectNoun2 = "Initialized";

                      String objNoun = "Initialized";

                      String objNoun2 = "Initialized";

                     

                      boolean correct = true; 

                      while (correct) {

                                 if (input == 0) {

                                    break;
                                 }

                                 if (input == 1) {

                                            System.out.print("The "); 

                                            adj1 = adjectives();

                                            System.out.print(adj1);

                                            adj2 = adjectives();

                                            while (adj1 == adj2) {

                                               adj2 = adjectives();

                                            }

                                            System.out.print(" " + adj2);

                                            System.out.print(" " + subjectNoun());

                                            System.out.print(" " + pastTenseVerbs());

                                            System.out.println(" " +objectNoun() + ".");

                                            System.out.println("If you want to quit, press 0. If you want another sentence, press 1.  If you want phase 2, press 2");                                         

                                            input = scan.nextInt(); 

                                 }

                                 if (input == 2) { 

                                            System.out.print("The "); 

                                            adj1 = adjectives();

                                            System.out.print(adj1);

                                            adj2 = adjectives();

                                            while (adj1 == adj2) {

                                                       adj2 = adjectives();

                                            }

                                            System.out.print(" " + adj2);

                                            subjectNoun = subjectNoun();

                                            System.out.print(" " + subjectNoun);

                                            System.out.print(" " + pastTenseVerbs());

                                            System.out.println(" " + objectNoun() + ".");

                                           

                                            subjectNoun2 = placementNoun(subjectNoun);

                                            System.out.print(subjectNoun2);

                                            System.out.print(pastTenseVerbs());

                                            objNoun = objectNoun();

                                            System.out.print(" " + objNoun);

                                            System.out.print(" at the");

                                            System.out.print(" " + adjectives());

                                            objNoun2 = objectNoun();

                                            while (objNoun == objNoun2) {

                                                       objNoun2 = objectNoun();

                                            }

                                            System.out.println(" " + objNoun2 + ".");

                                            conclusion(subjectNoun);

                                            System.out.println();

                                            System.out.println("The final method:");

                                            finalMethod(); 

                                                                                   

                                            System.out.println("If you want to quit, press 0. If you want another line, press 1.  If you want another phase 2, press 2");

                                            input = scan.nextInt(); 

                                }
                      }
           }
}