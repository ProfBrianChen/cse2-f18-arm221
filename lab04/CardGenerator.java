// CSE 2 Lab 4
// Arnella Margolin
// this program will pick a random card from the deck 


public class CardGenerator{
  
  public static void main(String args[]){
    int card = (int)(Math.random()*52)+1; //randoming picking card
    String suit = ""; // stores card suit
    if (card >= 1 && card <= 13){
       suit = "diamond"; //assigns cards 1-13 to a diamond
    }
    else if (card >= 14 && card <= 26){
       suit = "clubs"; // assigns cards 14-26 to a club
    }
    else if (card >= 27 && card <= 39){
       suit = "hearts"; // assigns cards 27-39 to a heart
    }
    else if(card >= 40 && card <= 52){
       suit = "spades";  // assigns cards 40-52 to a spade 
    }
    
    int remainder = (int)(card%13); // gets remainder value to identify card 
    String  cardIdentity = ""; //stores the value of the card 
    
    switch(remainder){
      case 1:
        cardIdentity = "Ace"; 
        break; 
      case 2:
        cardIdentity = "2";
        break; 
      case 3:
        cardIdentity = "3";
        break;
      case 4:
        cardIdentity= "4";
        break;
      case 5: 
        cardIdentity = "5"; 
        break; 
      case 6:
        cardIdentity = "6";
        break; 
      case 7:
        cardIdentity = "7";
        break;
      case 8:
        cardIdentity = "8";
        break; 
      case 9:
        cardIdentity = "9";
        break; 
      case 10:
        cardIdentity = "10";
        break; 
      case 11:
        cardIdentity = "Jack";
        break; 
      case 12:
        cardIdentity = "Queen";
        break; 
      case 13:
        cardIdentity = "King";
        break; 
    }
      System.out.println("You picked the " + cardIdentity + " of " +suit); //prints out the random card
    
  }
}