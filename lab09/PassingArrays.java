// Arnella Margolin
// CSE 2 Lab 09 Passing Arrays 
// this program illustrates the effect of passing arrays as method arguments


public class PassingArrays{
  
  public static int[] copy(int[] list){ //makes a copy of the literal array that is inputed in the main method
    int[] list2 = new int[list.length];
    for (int i=0; i<list.length; i++){ //Go through all of list and copy every value the member with the same index in list 2
	    list2[i] = list[i];
}
    return list2;
  }
  
  public static void inverter(int[] list){ //inverts the first array 
    int i = 0;
    for (i=0; i<list.length/2; i++){ 
     int temp = list[i]; //flips the positions of the array 
     list[i] = list[list.length-1-i];
     list[list.length-1-i] = temp;
	}
}
  
  public static int[] inverter2(int[] list){ //inverts the second array so it is basically the initial array you put in
    int[] copy = copy(list);
    int i = 0;
    for (i=0; i<copy.length/2; i++){
     int mix = copy[i];
     copy[i] = copy[copy.length-1-i];
     copy[copy.length-1-i] = mix;
	}
    return copy;
  }
  
  public static void print(int[] list){ //prints each array 
    for (int k=0; k<list.length; k++){
      System.out.print(list[k]+ " ");
    }
    System.out.println();
  }
  
  public static void main(String[] args){
    int[] array0 = {1,2,3,4,5,6,7,8}; //declares literal array 
    
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
  
}