// Arnella Margolin
// CSE 2 Hw 6 
// program runs based on users input to create a secret message "X" that is hidden between stars. The size varies based on the users input to determine the size of the square where the X will be. 

import java.util.Scanner;
public class EncryptedX{
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner ( System.in );
    System.out.print("Type integer between 0 and 100: "); //requests input from user on the size of square
    
    boolean correct = true;
    int inputNum = 0; //initiates the input number
    
    //check to see if the input is an integer between 0-100 
    if (myScanner.hasNextInt()){ //if the input is an integer
      inputNum = myScanner.nextInt(); //sets variable equal to the input 
      if (inputNum < 0 || inputNum > 100){ //checks to see if the number is out of range 
        correct = false; //the condition isn't met 
      }
    }
    else{
      correct = false; 
    }
    
    while(!correct){ //while the condition is not true
      myScanner.nextLine();
      System.out.println("Not correct! Type a new integer between 0-100: ");
      if (myScanner.hasNextInt()){ //if returned correctly this time
        inputNum = myScanner.nextInt(); //sets variable equal to correct input
        if(inputNum < 0 || inputNum > 100){
          correct = false;
          }
      else{
          correct = true; 
        }
      }
    }
    
    //encrypt the X 
    for (int i = 0; i < inputNum; i++){ 
      for(int j = 0; j < inputNum; j++){
        if(i == j || i == (inputNum - (j + 1 ))){ 
          System.out.print(" ");
        }
        else {
          System.out.print("*");
        }
      }
      System.out.println();
    }
      
  }  
}