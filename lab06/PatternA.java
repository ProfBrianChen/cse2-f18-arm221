//Arnella Margolin 
//CSE 2 Lab 6 10/11/18
// prints pattern type A based on the amount of lines determined my user input 

import java.util.Scanner;

public class PatternA{
  
  public static void main(String[] args){ 
  Scanner myScanner = new Scanner( System.in ); //scanner to initiate inouts
  
  boolean correct = true; 
  
  System.out.print("Enter an integer from 1 - 10: "); //asks user for input 
  int input = 0; //assigns variable to the user input 
  
     if (myScanner.hasNextInt()){

                   input = myScanner.nextInt(); // sets input equal to variable 

                 } else {

                   correct = false; // tests whether the input is the correct type

                 }

                 while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type

                   myScanner.next();

                   System.out.println("Error: wrong type! Enter integer from 1 to 10: " ); //tells user there is an error with their type and asks them for new input

                   if (myScanner.hasNextInt()){ // checking to see if input is the right type

                     input = myScanner.nextInt(); // sets input equal to variable for course number

                     correct = true; // if its true, it breaks out of the while loop

                 }

                 }
       while (input < 1 || input > 10){
         System.out.println("Error: out of range! Enter integer between 1 and 10: ");
         input = myScanner.nextInt();
       }
    
      int numColumns;
      for(int numRows = 1; numRows <= input; numRows++){
        for(numColumns = 1; numColumns <= numRows; numColumns++){
          System.out.print(numColumns + "");
        }
        System.out.println();
       }
  
  }
}